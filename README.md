# 大事件后台接口

#### 介绍
这是我的大事件后台完整的接口项目

#### 软件架构
软件架构说明
这是一个基于node的后台接口项目

#### 安装教程

1.  首先安装visual studio code等编译器
2.  然后安装mysql数据
3.  最后安装node服务器包

#### 使用说明

1.  这是基于node的项目，里面有包文件，所以需要npm包管理工具
2.  这只是一个后台项目，需要搭配我的大事件前端项目一起使用

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
