/*
 Navicat Premium Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 50088
 Source Host           : localhost:3306
 Source Schema         : breaking_news

 Target Server Type    : MySQL
 Target Server Version : 50088
 File Encoding         : 65001

 Date: 23/05/2022 10:18:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ev_article_cate
-- ----------------------------
DROP TABLE IF EXISTS `ev_article_cate`;
CREATE TABLE `ev_article_cate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE INDEX `name` USING BTREE(`name`),
  UNIQUE INDEX `alias` USING BTREE(`alias`)
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'InnoDB free: 10240 kB' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ev_article_cate
-- ----------------------------
INSERT INTO `ev_article_cate` VALUES (1, '工作', 'work', 0);
INSERT INTO `ev_article_cate` VALUES (2, '历史', 'LiShi', 0);
INSERT INTO `ev_article_cate` VALUES (3, '教育', 'education', 0);
INSERT INTO `ev_article_cate` VALUES (4, '体育', 'sports', 0);
INSERT INTO `ev_article_cate` VALUES (5, '娱乐', 'recreation', 0);
INSERT INTO `ev_article_cate` VALUES (6, '战争', 'warfare', 0);

-- ----------------------------
-- Table structure for ev_articles
-- ----------------------------
DROP TABLE IF EXISTS `ev_articles`;
CREATE TABLE `ev_articles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cover_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pub_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0,
  `cate_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'InnoDB free: 10240 kB' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ev_articles
-- ----------------------------
INSERT INTO `ev_articles` VALUES (1, '以色列爆发内乱', '以色列国家为了巩固政权发生了全国性的内乱', '\\uploads\\447f965a33e1c093679e9e4e41c83531', '2022-05-22 21:44:38.875', '已发布', 1, 6, 4);
INSERT INTO `ev_articles` VALUES (2, '俄乌战争', '俄罗斯和乌克兰之间爆发了战争', '\\uploads\\49b71aca50b409290aecf9347854a1c4', '2022-05-22 22:38:53.676', '已发布', 0, 6, 4);
INSERT INTO `ev_articles` VALUES (3, '三星堆', '四川发现了三星堆', '\\uploads\\7383c52f0caf89aba087981d96647e53', '2022-05-22 23:02:26.199', '已发布', 0, 2, 4);
INSERT INTO `ev_articles` VALUES (4, '你不知道的工作技巧', '你不知道的一些工作上的技巧', '\\uploads\\de13c070c117d7a06067ee0067908c60', '2022-05-22 23:03:09.375', '已发布', 0, 1, 4);
INSERT INTO `ev_articles` VALUES (5, '孩子的教育', '孩子的教育要这样教', '\\uploads\\f2194685a4a70059e485294f87daae09', '2022-05-22 23:03:48.548', '已发布', 0, 3, 4);
INSERT INTO `ev_articles` VALUES (6, '国足', '国足无药可救，建议解散国足', '\\uploads\\be1f92d05489ae79fc76fa5043cd60e5', '2022-05-22 23:04:25.124', '已发布', 0, 4, 4);
INSERT INTO `ev_articles` VALUES (7, '震惊！离婚了', '李小璐出轨了！', '\\uploads\\de91d5700973480bf8dd17ed7672e176', '2022-05-22 23:05:11.971', '已发布', 0, 5, 4);

-- ----------------------------
-- Table structure for ev_users
-- ----------------------------
DROP TABLE IF EXISTS `ev_users`;
CREATE TABLE `ev_users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `user_pic` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE INDEX `username` USING BTREE(`username`)
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'InnoDB free: 10240 kB' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ev_users
-- ----------------------------
INSERT INTO `ev_users` VALUES (1, 'zs', '$2a$10$ofNhIyBPRdntaojwCV2xZ.D2pm7WonJgNGGCfWyGYGCrPPmb7CW0C', NULL, NULL, NULL);
INSERT INTO `ev_users` VALUES (2, 'lisi', '$2a$10$f1jxV1YYSBKl4jKsr7y8g.eXdquZDHsoFwfaSGEIOqfxSoGOyIJIG', NULL, NULL, NULL);
INSERT INTO `ev_users` VALUES (3, 'wangwu', '$2a$10$2/V8rdBysssqJyRk4MQzxOSQnJKiXdES3CvEbpxAX5drhAAOxEdUS', NULL, NULL, NULL);
INSERT INTO `ev_users` VALUES (4, 'roote', '123456', NULL, NULL, 'data:image/png;base64,VE9PTUFOWVNFQ1JFVFM=');

SET FOREIGN_KEY_CHECKS = 1;
