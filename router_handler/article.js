// 导入处理路径的核心模块
const path = require('path')
// 导入数据库操作模块
const db = require('../db/index')
// 路由函数处理模块
// 发布文章的处理函数
exports.addArticle = (req, res) => {
  // 手动判断是否上传了文章封面
  if (!req.file || req.file.fieldname !== 'cover_img') return res.cc('文章封面是必选参数')
  // 整理要插入到数据库的文章信息对象
  const articleInfo = {
    // 标题、内容、状态、所属的分类id
    ...req.body,
    // 文章封面在服务器端的存放路径 
    cover_img: path.join('/uploads', req.file.filename),
    // 文章发布事件
    pub_date: new Date(),
    // 文章作者的id
    author_id: req.user.id
  }
  // 定义发布文章的事情了语句
  let sql = 'insert into ev_articles set ?'
  // 执行 SQL 语句
  db.query(sql, articleInfo, (err, results) => {
    // 执行 sql 语句失败
    if (err) return res.cc(err)
    // 执行 SQL 语句成功 但是影响行数不为1
    if (results.affectedRows !== 1) return res.cc('发布文章失败！')
    // 发布文章成功
    res.cc('发布文章成功！', 0)
  })
}

// 获取文章列表的处理函数
exports.getArticle = (req, res) => {
  // 解构赋值
  let {pagenum,pagesize,cate_id,state} = req.body
  // 定义数据数组
  let data = []
  // 定义获取文章列表的处理函数
  let sql = `select ev_articles.id,
  ev_articles.title,
  ev_articles.pub_date,
  ev_articles.state,
  ev_article_cate.name as cate_name
  from ev_articles 
  inner join ev_article_cate 
  on ev_articles.cate_id = ev_article_cate.id 
  where ev_articles.is_delete = 0`
  // 判断可选参数是否有值
  if (cate_id) {
    sql += ` and cate_id=${cate_id}`
  }
  if (state) {
    sql += ` and state='${state}'`
  }
  // 执行SQL语句
  db.query(sql, (err, results) => {
    // 执行SQL语句失败
    if (err) return res.cc(err)
    // 总页数
    let total = Math.ceil(results.length / pagesize)
    // 当前页面起始条数
    let start_data = pagesize * (pagenum - 1)
    for (let i = start_data; i < start_data + pagesize; i++) {
      data.push(results[i])
    }
    res.send({
      status: 0,
      message: '获取文章列表成功！',
      data,
      total
    })
  })
}

// 根据id删除文章数据
exports.deleteArticle = (req,res) => {
  // 定义 SQL 语句
  let sql = 'update ev_articles set is_delete=1 where id= ?'
  // 执行 SQL 语句
  db.query(sql,req.params.id,(err,results)=>{
    // 执行 SQL 语句失败
    if(err) return res.cc(err)
    // 执行成功 但是 影响条数不为1
    if(results.affectedRows!==1) return res.cc('删除文章数据失败！')
    // 删除文章数据成功
    res.cc('删除文章数据成功！',0)
  })
}

// 根据id获取文章详细信息
exports.getArticleById = (req,res)=>{
  // 定义SQL语句
  let sql = 'select * from ev_articles where id= ?'
  // 执行 SQL 语句
  db.query(sql,req.params.id,(err,results)=>{
    // 执行SQL 语句失败
    if(err) return res.cc(err)
    // 执行SQL 语句成功，但是数据条数不为1
    if(results.length!==1) return res.cc('获取文章信息失败！')
    // 获取信息成功
    res.send({
      status:0,
      message:'获取文章信息成功！',
      data: results[0]
    })
  })
}

// 根据id更新文章信息
exports.editArticleById = (req,res)=>{
  // 手动判断文章封面是否上传
  if(!req.file||req.file.fieldname!=='cover_img') return res.cc('文章封面是必选参数')
  // 整理要更新到数据库的文章信息对象
  const articleInfo = {
    // 标题、内容、状态、所属的分类id
    ...req.body,
    // 文章封面在服务器端的存放路径 
    cover_img: path.join('/uploads', req.file.filename),
  }
  // 定义SQL语句
  let sql = 'update ev_articles set ? where id = ?'
  // 执行SQL语句
  db.query(sql,[articleInfo,articleInfo.id],(err,results)=>{
    // 执行SQL语句失败
    if(err) return res.cc(err)
    // 执行SQL 语句成功 但是影响行数不为1
    if(results.affectedRows!==1) return res.cc('更新文章信息失败！')
    // 更新文章信息成功
    res.cc('更新文章信息成功！',0)
  })
}