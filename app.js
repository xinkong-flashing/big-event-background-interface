// 导入 express 模块
const express = require('express')
// 导入 cors 中间件
const cors = require('cors')
// 导入验证规则中间件
const joi = require('joi')
// 创建 express 服务器的实例对象
const app = express()
// 导入解析 token 的中间件
const expressJWT = require('express-jwt')
// 导入配置文件
const config = require('./config/config')
// 导入用户路由模块
const userRouter = require('./router/user')
// 导入信息路由模块
const userinfoRouter = require('./router/userinfo')
// 导入文章列表路由模块
const artcateRouter = require('./router/artcate')
// 导入文章路由模块
const articleRouter = require('./router/article')

// 将 cors 注册为全局中间件
app.use(cors())
// 配置解析表单数据中间件
app.use(express.urlencoded({ extended: false }))
// 注册响应数据的中间件
app.use((req,res,next)=>{
  // status = 0 为成功； status = 1 为失败； 默认将 status 的值设置为 1，方便处理失败的情况
  res.cc=(err,status=1)=>{
    res.send({
      // 状态
      status,
      // 状态描述
      message: err instanceof Error?err.message:err
    })
  }
  next()
})
// 使用 .unless({ path: [/^\/api\//] }) 指定哪些接口不需要进行 Token 的身份认证
app.use(expressJWT({ secret: config.jwtSecretKey }).unless({ path: [/^\/api\//] }))
// 注册用户路由模块
app.use('/api',userRouter)
// 注册个人中心路由模块
// 注意 以 /my 开头的接口，都是有权限的接口，需要进行Token身份认证
app.use('/my',userinfoRouter)
// 注册文章列表路由模块
app.use('/my/article',artcateRouter)
// 注册文章路由模块
app.use('/my/article',articleRouter)
// 托管静态资源文件
app.use('/uploads',express.static('./uploads'))
// 定义全局错误中间件
app.use((err,req,res,next)=>{
  // 数据验证失败
  if(err instanceof joi.ValidationError){
    return res.cc(err)
  }
  // 捕获身份认证失败的错误
  if(err.name == 'UnauthorizedError') return res.cc('身份认证失败！')
  // 未知错误
  res.cc(err)
})

// 调用 app.listen 方法，配置端口号并启动服务器
app.listen(8080,()=>{
  console.log('express sever running at http://127.0.0.1:8080');
})