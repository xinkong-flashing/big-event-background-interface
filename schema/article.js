// 导入定义验证规则的包
const joi = require('joi')

// 定义 标题、分类id、内容、分发布状态的验证规则
const title = joi.string().required()
const cate_id = joi.number().integer().min(1).required()
const content = joi.string().required().allow('')
const state = joi.string().valid('已发布','草稿').required()

// 定义 页码值 每页显示多少条数据 文章分类的id 
const pagenum = cate_id
const pagesize = cate_id
const list_cate_id = joi.string()
const list_state = joi.string().valid('已发布','草稿')

// 验证规则对象 - 发布文章
exports.add_article_schema={
  body:{
    title,
    cate_id,
    content,
    state
  }
}

// 验证规则对象 - 获取文章列表
exports.get_article_schema={
  body:{
    pagenum,
    pagesize,
    cate_id:list_cate_id,
    state:list_state
  }
}

// 验证规则对象 - 根据id删除文章
exports.delete_article_schema={
  params:{
    id:cate_id
  }
}

// 验证规则对象 - 根据id获取文章详细信息
exports.get_articleById_schema={
  params:{
    id:cate_id
  }
}

// 验证规则对象 - 根据id更新文章信息
exports.edit_articleById_schema={
  body:{
    id:cate_id,
    title,
    cate_id,
    content,
    state
  }
}