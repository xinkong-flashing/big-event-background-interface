// 导入 express 服务器模块
const express = require('express')
// 创建路由对象
const router = express.Router()
// 导入验证表单数据中间件
const expressJoi = require('@escook/express-joi')
// 导入验证规则模块
const {add_cate_schema, delete_cate_schema, get_cate_schema, update_cate_schema} = require('../schema/artcate')
// 导入路由函数处理模块
const router_handler = require('../router_handler/artcate')

// 获取文章分类列表的数据
router.get('/cates',router_handler.getArticleCates)

// 新增文章分类
router.post('/addcates',expressJoi(add_cate_schema),router_handler.addArticleCates)

// 删除文章分类
router.get('/deletecate/:id',expressJoi(delete_cate_schema),router_handler.deleteCateById)

// 根据 id 获取文章分类数据
router.get('/cates/:id',expressJoi(get_cate_schema),router_handler.getArtCateById)

// 根据 id 更新文章分类数据
router.post('/updatecate',expressJoi(update_cate_schema),router_handler.updateCateById)
// 向外共享路由对象
module.exports=router