// 导入 express 模块
const express = require('express')
// 导入解析 formdata 格式表单数据的包
const multer = require('multer')
// 导入路径处理的核心模块
const path = require('path')
// 导入验证数据的中间件
const expressJoi = require('@escook/express-joi')
// 创建路由对象
const router = express.Router()
// 创建 multer 的实例对象，通过 dest 属性指定文件的存放路径
const upload = multer({dest:path.join(__dirname,'../uploads/')})
// 导入路由函数处理模块
const router_handler = require('../router_handler/article')
// 导入文章的验证模块
const {add_article_schema, get_article_schema, delete_article_schema, get_articleById_schema, edit_articleById_schema} = require('../schema/article')

// 发布新文章的路由
// upload.single() 是一个局部生效的中间件，用来解析 FormData 格式的表单数据
// 将文件类型的数据，解析并挂载到 req.file 属性中
// 将文本类型的数据，解析并挂载到 req.body 属性中
router.post('/add',upload.single('cover_img'),expressJoi(add_article_schema),router_handler.addArticle)

// 获取文章列表的数据
router.get('/list',expressJoi(get_article_schema),router_handler.getArticle)

// 根据 id 删除文章数据
router.get('/delete/:id',expressJoi(delete_article_schema),router_handler.deleteArticle)

// 根据 id 获取文章详细信息
router.get('/:id',expressJoi(get_articleById_schema),router_handler.getArticleById)

// 根据 Id 更新文章信息
router.post('/edit',upload.single('cover_img'),expressJoi(edit_articleById_schema),router_handler.editArticleById)

// 向外共享路由对象
module.exports = router